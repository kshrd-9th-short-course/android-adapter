package com.chanchhaya.androidadapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chanchhaya.androidadapter.R;

import java.util.ArrayList;

public class RecyclerDataAdapter extends RecyclerView.Adapter<RecyclerDataAdapter.DataViewHolder> {

    private final ArrayList<String> dataSetCountryName;
    private final ArrayList<Integer> dataSetCountryImage;
    private final LayoutInflater layoutInflater;

    public RecyclerDataAdapter(Context context, ArrayList<String> dataSetCountryName,
                               ArrayList<Integer> dataSetCountryImage) {
        layoutInflater = LayoutInflater.from(context);
        this.dataSetCountryName = dataSetCountryName;
        this.dataSetCountryImage = dataSetCountryImage;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.country_list_view_item,
                parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.imageView.setImageResource(dataSetCountryImage.get(position));
        holder.textView.setText(dataSetCountryName.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSetCountryName.size();
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imageView;
        private final TextView textView;

        public DataViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_country);
            textView = itemView.findViewById(R.id.text_view_country);
        }
    }

}
