package com.chanchhaya.androidadapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chanchhaya.androidadapter.R;
import com.chanchhaya.androidadapter.model.Book;

import java.util.ArrayList;
import java.util.List;

public class AllBookAdapter extends RecyclerView.Adapter<AllBookAdapter.BookViewHolder> {

    private List<Book> books = new ArrayList<>();
    private final LayoutInflater layoutInflater;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public AllBookAdapter(Context context, List<Book> books) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.books = books;
        this.onItemClickListener = (OnItemClickListener) context;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.all_books_item_layout, parent, false);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        //holder.imageCover.setImageResource(R.drawable.cambodia);
        Glide
                .with(context)
                .load(books.get(position).getCoverUrl())
                .centerCrop()
                .into(holder.imageCover);
        holder.textTitle.setText(books.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final AppCompatImageView imageCover;
        private final AppCompatTextView textTitle;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            imageCover = itemView.findViewById(R.id.image_book);
            textTitle = itemView.findViewById(R.id.text_book_title);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            onItemClickListener.onItemClick(position);
        }

    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

}
