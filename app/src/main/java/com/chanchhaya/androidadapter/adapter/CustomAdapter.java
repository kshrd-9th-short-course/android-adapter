package com.chanchhaya.androidadapter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.chanchhaya.androidadapter.R;

public class CustomAdapter extends BaseAdapter {

    private Context context;
    private String[] countryName;
    private int[] countryFlag;
    private LayoutInflater layoutInflater;

    public CustomAdapter(Context context, String[] countryName, int[] countryFlag) {
        this.context = context;
        this.countryName = countryName;
        this.countryFlag = countryFlag;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return countryName.length;
    }

    @Override
    public Object getItem(int position) {
        return countryName[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = layoutInflater.inflate(R.layout.country_list_view_item, null);

        TextView textViewCountryName = convertView.findViewById(R.id.text_view_country);
        ImageView imageCountryFlag = convertView.findViewById(R.id.image_country);

        textViewCountryName.setText(countryName[position]);
        imageCountryFlag.setImageResource(countryFlag[position]);

        return convertView;
    }

}
