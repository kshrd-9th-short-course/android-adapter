package com.chanchhaya.androidadapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.chanchhaya.androidadapter.adapter.AllBookAdapter;
import com.chanchhaya.androidadapter.model.Book;

import java.util.ArrayList;
import java.util.List;

import io.bloco.faker.Faker;

public class BookActivity extends AppCompatActivity implements AllBookAdapter.OnItemClickListener {

    private RecyclerView recyclerAllBooks;
    private LinearLayoutManager layoutManager;
    private AllBookAdapter allBookAdapter;

    private List<Book> books = new ArrayList<>();

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        recyclerAllBooks = findViewById(R.id.recycler_all_books);

        Faker faker = new Faker();
        for (int i = 0; i < 100; i++) {
            String coverUrl = faker.avatar.image();
            Log.i("MYTAG", coverUrl);
            books.add(new Book(i, faker.book.title(), coverUrl));
        }

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        allBookAdapter = new AllBookAdapter(this, books);

        recyclerAllBooks.setLayoutManager(layoutManager);
        recyclerAllBooks.setAdapter(allBookAdapter);

    }

    @Override
    public void onItemClick(int position) {
        Log.i("MYTAG", books.get(position).getTitle());
    }

}