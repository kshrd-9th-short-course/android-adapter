package com.chanchhaya.androidadapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.chanchhaya.androidadapter.adapter.RecyclerDataAdapter;

import java.util.ArrayList;

public class SimpleDataActivity extends AppCompatActivity {

    private RecyclerView recyclerData;
    private RecyclerDataAdapter recyclerDataAdapter;

    private final ArrayList<String> countryName = new ArrayList<>();
    private final ArrayList<Integer> countryImage = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_data);

        // init data set
        for (int i = 0; i < 100; i++) {
            countryName.add("Country#0" + i);
            countryImage.add(R.drawable.cambodia);
        }

        // init recycler view
        recyclerData = findViewById(R.id.recycler_data);

        // define layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        // init adapter for recycler view
        recyclerDataAdapter = new RecyclerDataAdapter(this, countryName, countryImage);

        recyclerData.setLayoutManager(layoutManager);
        recyclerData.setAdapter(recyclerDataAdapter);

        // 1. Recycler View (activity layout)
        // 2. Recycler View Item Layout (custom layout)
        // 3. Define Adapter
            // 1. Extend RecyclerView.Adapter => Overriding method
            // 2. Define inner class ViewHolder (extend from RecyclerView.ViewHolder)
        // 4. Instantiate adapter object
        // 5. Instantiate layout manager object
        // 6. Set adapter and layout manager object to recycler view object


    }

}