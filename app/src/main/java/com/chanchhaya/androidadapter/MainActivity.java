package com.chanchhaya.androidadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    // Data source
    private String[] footballers = new String[200];
    private ListView listViewData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listViewData = findViewById(R.id.list_view_data);

        for (int i = 0; i < 20; i++) {
            footballers[i] = "Footballer#00" + (i + 1);
        }

        // Create array adapter
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.list_view_item,
                R.id.text_view_data, footballers);

        // Set adapter to list view
        listViewData.setAdapter(arrayAdapter);

    }

}