package com.chanchhaya.androidadapter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.chanchhaya.androidadapter.adapter.CustomAdapter;

public class ListCountryActivity extends AppCompatActivity {

    // Data source
    private String[] countries = {
            "Cambodia",
            "Thailand",
            "Korea",
            "America",
            "Canada",
            "UK",
            "Singapore",
            "HongKong",
            "China",
            "Spain",
    };

    private int[] flags = {
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
            R.drawable.cambodia,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_country);

        ListView countryListView = findViewById(R.id.country_list_view);

        // create object of custom adapter
        CustomAdapter adapter = new CustomAdapter(this, countries, flags);

        countryListView.setAdapter(adapter);

    }
}